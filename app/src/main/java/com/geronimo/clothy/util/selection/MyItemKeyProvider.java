package com.geronimo.clothy.util.selection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import androidx.recyclerview.selection.ItemKeyProvider;

public class MyItemKeyProvider extends ItemKeyProvider {
    private final List<Object> itemList;

    public MyItemKeyProvider(int scope, List<Object> itemList) {
        super(scope);
        this.itemList = itemList;
    }

    @Nullable
    @Override
    public Object getKey(int position) {
        return itemList.get(position);
    }

    @Override
    public int getPosition(@NonNull Object key) {
        return itemList.indexOf(key);
    }
}
