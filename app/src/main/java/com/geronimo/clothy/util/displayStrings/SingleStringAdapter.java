package com.geronimo.clothy.util.displayStrings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.geronimo.clothy.AppData;
import com.geronimo.clothy.R;
import com.geronimo.clothy.data.Configuration;
import com.geronimo.clothy.util.displayConfigurations.ConfigComparator;
import com.geronimo.clothy.util.displayConfigurations.ConfigViewHolder;

import java.util.List;

import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

/**
 * Created by Ramona on 23.03.2018.
 */

public class SingleStringAdapter extends RecyclerView.Adapter<SingleStringViewHolder>{
    private SortedList<String> sortedList;

    // Provide a suitable constructor (depends on the kind of dataset)
    public SingleStringAdapter(List<String> listItems) {
        initSortedList();
        add(listItems);
    }

    private void initSortedList(){
        this.sortedList = new SortedList<>(String.class, new SortedList.Callback<String>() {
            @Override
            public int compare(String a, String b) {
                return a.compareTo(b);
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(String oldItem, String newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areItemsTheSame(String item1, String item2) {
                return item1.equals(item2);
            }
        });
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return sortedList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SingleStringViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SingleStringViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.list_single_string,
                        parent,
                        false));
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(SingleStringViewHolder holder, int position) {
        holder.bind(getItem(position));
    }


    public String getItem(int position){
        return this.sortedList.get(position);
    }


    public void add(String model) {
        this.sortedList.add(model);
    }

    public void remove(String model) {
        this.sortedList.remove(model);
    }

    public void add(List<String> models) {
        this.sortedList.addAll(models);
    }

    public void remove(List<String> models) {
        this.sortedList.beginBatchedUpdates();
        for (String model : models) {
            this.sortedList.remove(model);
        }
        this.sortedList.endBatchedUpdates();
    }

    public void renewList(List<String> models) {
        this.sortedList.beginBatchedUpdates();
        this.sortedList.clear();
        this.sortedList.addAll(models);
        this.sortedList.endBatchedUpdates();
    }

    public void replaceAll(List<String> models) {
        this.sortedList.beginBatchedUpdates();
        for (int i = this.sortedList.size() - 1; i >= 0; i--) {
            final String model = this.sortedList.get(i);
            if (!models.contains(model)) {
                this.sortedList.remove(model);
            }
        }
        this.sortedList.addAll(models);
        this.sortedList.endBatchedUpdates();
    }
}
