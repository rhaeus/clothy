package com.geronimo.clothy.util.selection;

import androidx.annotation.Nullable;

import androidx.recyclerview.selection.ItemDetailsLookup;

public class MyItemDetail extends ItemDetailsLookup.ItemDetails<Long> {
    private final int adapterPosition;
    private final long selectionKey;

    public MyItemDetail(int adapterPosition, long id) {
        this.adapterPosition = adapterPosition;
        this.selectionKey = id;
    }

    @Override
    public int getPosition() {
        return adapterPosition;
    }

    @Nullable
    @Override
    public Long getSelectionKey() {
        return selectionKey;
    }
}