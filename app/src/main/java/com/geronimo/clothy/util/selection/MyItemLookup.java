package com.geronimo.clothy.util.selection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.MotionEvent;
import android.view.View;

import com.geronimo.clothy.util.displayConfigurations.ConfigViewHolder;

import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.widget.RecyclerView;

public class MyItemLookup extends ItemDetailsLookup<Long> {

    private final RecyclerView recyclerView;

    public MyItemLookup(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    @Nullable
    @Override
    public ItemDetails<Long> getItemDetails(@NonNull MotionEvent e) {
        View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
        if (view != null) {
            RecyclerView.ViewHolder viewHolder = recyclerView.getChildViewHolder(view);
            if (viewHolder instanceof ConfigViewHolder) {
                return ((ConfigViewHolder) viewHolder).getItemDetails();
            }
        }

        return null;
    }
}
