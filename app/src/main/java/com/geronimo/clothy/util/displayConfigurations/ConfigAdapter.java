package com.geronimo.clothy.util.displayConfigurations;

import androidx.recyclerview.widget.SortedList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.geronimo.clothy.AppData;
import com.geronimo.clothy.R;
import com.geronimo.clothy.data.Configuration;

import java.util.List;

import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Ramona on 23.03.2018.
 */

public class ConfigAdapter extends RecyclerView.Adapter<ConfigViewHolder>{
    private SelectionTracker selectionTracker;
    private SortedList<Configuration> sortedList;

    //private Context context;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ConfigAdapter(List<Configuration> listItems, Context context) {
        initSortedList();
        add(listItems);

        //this.context = context;

        setHasStableIds(true);
    }

    private void initSortedList(){
        this.sortedList = new SortedList<>(Configuration.class, new SortedList.Callback<Configuration>() {
            @Override
            public int compare(Configuration a, Configuration b) {
                return new ConfigComparator(AppData.getManager().getLastSortOrder()).compare(a,b);
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(Configuration oldItem, Configuration newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areItemsTheSame(Configuration item1, Configuration item2) {
                return item1.getId() == item2.getId();
            }
        });
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return sortedList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
        //return this.sortedList.get(position).getId(); funktioniert so noch nicht
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ConfigViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ConfigViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.list_two_lines,
                        parent,
                        false));
//        // create a new view
//        View itemView = LayoutInflater.
//                from(parent.getContext()).
//                inflate(R.layout.list_two_lines, parent, false);
//
//        final ItemViewHolder mViewHolder = new ItemViewHolder(itemView);
//        itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listener.onItemClick(v, mViewHolder.getLayoutPosition());
//            }
//        });
//        return mViewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ConfigViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Configuration item = getItem(position);

        //selection
        holder.bind(item, selectionTracker.isSelected(getItemId(position)));
    }


    public Configuration getItem(int position){
        return this.sortedList.get(position);
    }


    public void add(Configuration model) {
        this.sortedList.add(model);
    }

    public void remove(Configuration model) {
        this.sortedList.remove(model);
    }

    public void add(List<Configuration> models) {
        this.sortedList.addAll(models);
    }

    public void remove(List<Configuration> models) {
        this.sortedList.beginBatchedUpdates();
        for (Configuration model : models) {
            this.sortedList.remove(model);
        }
        this.sortedList.endBatchedUpdates();
    }

    public void renewList(List<Configuration> models) {
        this.sortedList.beginBatchedUpdates();
        this.sortedList.clear();
        this.sortedList.addAll(models);
        this.sortedList.endBatchedUpdates();
    }

    public void replaceAll(List<Configuration> models) {
        this.sortedList.beginBatchedUpdates();
        for (int i = this.sortedList.size() - 1; i >= 0; i--) {
            final Configuration model = this.sortedList.get(i);
            if (!models.contains(model)) {
                this.sortedList.remove(model);
            }
        }
        this.sortedList.addAll(models);
        this.sortedList.endBatchedUpdates();
    }


    public void setSelectionTracker(SelectionTracker selectionTracker) {
        this.selectionTracker = selectionTracker;
    }
}
