package com.geronimo.clothy.util.displayDetergents;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.geronimo.clothy.AppData;
import com.geronimo.clothy.R;
import com.geronimo.clothy.data.Configuration;
import com.geronimo.clothy.data.Detergent;
import com.geronimo.clothy.util.displayConfigurations.ConfigComparator;
import com.geronimo.clothy.util.displayConfigurations.ConfigViewHolder;

import java.util.List;

import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

/**
 * Created by Ramona on 23.03.2018.
 */

public class DetergentAdapter extends RecyclerView.Adapter<DetergentViewHolder>{
    private SortedList<Detergent> sortedList;

    // Provide a suitable constructor (depends on the kind of dataset)
    public DetergentAdapter(List<Detergent> listItems) {
        initSortedList();
        add(listItems);
    }

    private void initSortedList(){
        this.sortedList = new SortedList<>(Detergent.class, new SortedList.Callback<Detergent>() {
            @Override
            public int compare(Detergent a, Detergent b) {
                return a.compareTo(b);
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(Detergent oldItem, Detergent newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areItemsTheSame(Detergent item1, Detergent item2) {
                return item1.equals(item2);
            }
        });
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return sortedList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DetergentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DetergentViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.list_two_lines,
                        parent,
                        false));
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(DetergentViewHolder holder, int position) {
        holder.bind(getItem(position));
    }


    public Detergent getItem(int position){
        return this.sortedList.get(position);
    }


    public void add(Detergent model) {
        this.sortedList.add(model);
    }

    public void remove(Detergent model) {
        this.sortedList.remove(model);
    }

    public void add(List<Detergent> models) {
        this.sortedList.addAll(models);
    }

    public void remove(List<Detergent> models) {
        this.sortedList.beginBatchedUpdates();
        for (Detergent model : models) {
            this.sortedList.remove(model);
        }
        this.sortedList.endBatchedUpdates();
    }

    public void renewList(List<Detergent> models) {
        this.sortedList.beginBatchedUpdates();
        this.sortedList.clear();
        this.sortedList.addAll(models);
        this.sortedList.endBatchedUpdates();
    }

    public void replaceAll(List<Detergent> models) {
        this.sortedList.beginBatchedUpdates();
        for (int i = this.sortedList.size() - 1; i >= 0; i--) {
            final Detergent model = this.sortedList.get(i);
            if (!models.contains(model)) {
                this.sortedList.remove(model);
            }
        }
        this.sortedList.addAll(models);
        this.sortedList.endBatchedUpdates();
    }
}
