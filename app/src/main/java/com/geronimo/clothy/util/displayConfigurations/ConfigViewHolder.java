package com.geronimo.clothy.util.displayConfigurations;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.geronimo.clothy.R;
import com.geronimo.clothy.data.Configuration;
import com.geronimo.clothy.util.selection.MyItemDetail;
import com.geronimo.clothy.util.selection.ViewHolderWithDetails;

import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.widget.RecyclerView;

// Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder
public class ConfigViewHolder extends RecyclerView.ViewHolder
        implements ViewHolderWithDetails {

    //public RelativeLayout ViewBackground;
    public RelativeLayout ViewForeground;
    protected TextView lineName;
    protected  TextView lineDetails;

    public ConfigViewHolder(View v) {
        super(v);

        //ViewBackground = v.findViewById(R.id.view_background);
        ViewForeground = v.findViewById(R.id.view_foreground);

        lineName = (TextView) v.findViewById(R.id.line_name);
        lineDetails = (TextView) v.findViewById(R.id.line_details);
    }

    @Override
    public ItemDetailsLookup.ItemDetails<Long> getItemDetails() {
        return new MyItemDetail(getAdapterPosition(), getItemId());
    }

    public final void bind(Configuration item, boolean isActive) {
        itemView.setActivated(isActive);

        lineName.setText(item.getName());
        lineDetails.setText(item.getDetails());
    }
}