package com.geronimo.clothy.util.displayDetergents;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.geronimo.clothy.R;
import com.geronimo.clothy.data.Detergent;

import androidx.recyclerview.widget.RecyclerView;

// Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder
public class DetergentViewHolder extends RecyclerView.ViewHolder {
    public RelativeLayout ViewForeground;
    protected TextView lineName;
    protected TextView lineDetails;

    public DetergentViewHolder(View v) {
        super(v);

        ViewForeground = v.findViewById(R.id.view_foreground);
        lineName = v.findViewById(R.id.line_name);
        lineDetails = v.findViewById(R.id.line_details);
    }
    public final void bind(Detergent item) {
        lineName.setText(item.getName());
        lineDetails.setText("Priorität: " + item.getPriority());
    }
}