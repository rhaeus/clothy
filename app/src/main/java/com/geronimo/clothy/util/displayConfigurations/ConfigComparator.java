package com.geronimo.clothy.util.displayConfigurations;

import com.geronimo.clothy.data.ConfigOrder;
import com.geronimo.clothy.data.Configuration;

import java.util.Comparator;

public class ConfigComparator implements Comparator<Configuration>{
    private ConfigOrder sortOrder;
    public ConfigComparator(ConfigOrder sortOrder){
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(Configuration o1, Configuration o2) {
        switch(sortOrder){
            case NAME_ASC:
                return o1.getName().compareTo(o2.getName());
            case NAME_DESC:
                return o2.getName().compareTo(o1.getName());
            case TEMP_ASC:
                return Integer.compare(o1.getTemperature(), o2.getTemperature());
            case TEMP_DESC:
                return Integer.compare(o2.getTemperature(), o1.getTemperature());
            case DETERGENT_ASC:
                return o1.getDetergent().compareTo(o2.getDetergent());
            case DETERGENT_DESC:
                return o2.getDetergent().compareTo(o1.getDetergent());
            default:
                return 0; //returns a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
        }
    }
}
