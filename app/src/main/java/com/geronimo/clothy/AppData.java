package com.geronimo.clothy;

import com.geronimo.clothy.data.Configuration;
import com.geronimo.clothy.data.Manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

// A top-level Java class mimicking static class behavior
public final class AppData { //final - Prevents extension of the class since extending a static class makes no sense

    private final static String APPDATA_FILE_NAME = "ClothyData.xml";
    private static Manager manager = new Manager();

    private AppData(){
    }

    public static Manager getManager(){
        return manager;
    }

    public static boolean readAppData(String filePath){
        Serializer serializer = new Persister();
        manager = new Manager(); //clear

        try {
            FileInputStream fileIn = new FileInputStream(filePath + "/" + APPDATA_FILE_NAME);
            manager = serializer.read(Manager.class, fileIn);
            //manager.generateDummyData();
            return true;
        }catch(Exception e){
            String s = e.getMessage();
            //manager.generateDummyData();
            return false;
        }
    }

    public static boolean writeAppData(String filePath){
        Serializer serializer = new Persister();
        File result = new File(filePath + "/" + APPDATA_FILE_NAME);

        try {
            FileOutputStream fos = new FileOutputStream(result);
            serializer.write(manager, fos);
            return true;
        }catch(Exception e){
            String s = e.getMessage();
            return false;
        }
    }
}
