package com.geronimo.clothy.fragments;

import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geronimo.clothy.AppData;
import com.geronimo.clothy.R;
import com.geronimo.clothy.data.Detergent;
import com.geronimo.clothy.util.spinner.CustomSpinner;

import java.util.ArrayList;
import java.util.List;

public class DialogAddConfiguration extends DialogFragment {
    private View dialogView;
    private TextView tvConfigDesc;
    private LinearLayout llDescError;

    private TextView tvTemp;
    private CustomSpinner cspTemperatures;
    private LinearLayout llTempError;

    private TextView tvDet;
    private CustomSpinner cspDetergents;
    private LinearLayout llDetError;

    /** The system calls this to get the DialogFragment's layout, regardless
     of whether it's being displayed as a dialog or an embedded fragment. */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        this.dialogView = inflater.inflate(R.layout.frag_add_config, container, false);
        initViews();

        //cancel icon
        ImageView cancel = this.dialogView.findViewById(R.id.cancelAddItem);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                cancel();
            }
        });

        //save icon
        TextView save = this.dialogView.findViewById(R.id.saveItem);
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                save();
            }
        });

        return this.dialogView;
    }

    private void initViews(){
        //config desc
        this.tvConfigDesc = this.dialogView.findViewById(R.id.config_desc);
        this.llDescError = this.dialogView.findViewById(R.id.ConfigDescErrorLayout);

        //temp
        initTempSpinner();

        //detergent
        initDetergentSpinner();
    }

    private void initTempSpinner(){
        this.cspTemperatures = this.dialogView.findViewById(R.id.spinnerTemp);
        this.llTempError = this.dialogView.findViewById(R.id.TempErrorLayout);

        List<String> temps = AppData.getManager().getTemperaturesWithUnit();

        if(temps.size() > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, temps.toArray(new String[temps.size()]));
            adapter.setDropDownViewResource(R.layout.spinner_item);
            this.cspTemperatures.setAdapter(adapter);
        }

        this.tvTemp = this.dialogView.findViewById(R.id.temp_label);
        this.cspTemperatures.setSpinnerEventsListener(new CustomSpinner.OnSpinnerEventsListener() {
            @Override
            public void onSpinnerOpened(AppCompatSpinner spinner) {
                tvTemp.setTextColor(getResources().getColor(R.color.colorAccent, getActivity().getTheme()));
            }

            @Override
            public void onSpinnerClosed(AppCompatSpinner spinner) {
                tvTemp.setTextColor(getResources().getColor(R.color.colorSecondaryText, getActivity().getTheme()));
            }
        });
    }

    private void initDetergentSpinner(){
        this.cspDetergents = this.dialogView.findViewById(R.id.spinnerDetergent);
        this.llDetError = this.dialogView.findViewById(R.id.DetergentErrorLayout);

        List<String> detNames = new ArrayList<>();

        List<Detergent> dets = AppData.getManager().getDetergents();

        for (Detergent det : dets){
            detNames.add(det.getName());
        }

        if(detNames.size() > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, detNames.toArray(new String[detNames.size()]));
            adapter.setDropDownViewResource(R.layout.spinner_item);
            this.cspDetergents.setAdapter(adapter);
        }

        this.tvDet = this.dialogView.findViewById(R.id.detergent_label);
        this.cspDetergents.setSpinnerEventsListener(new CustomSpinner.OnSpinnerEventsListener() {
            @Override
            public void onSpinnerOpened(AppCompatSpinner spinner) {
                tvDet.setTextColor(getResources().getColor(R.color.colorAccent, getActivity().getTheme()));
            }

            @Override
            public void onSpinnerClosed(AppCompatSpinner spinner) {
                tvDet.setTextColor(getResources().getColor(R.color.colorSecondaryText, getActivity().getTheme()));
            }
        });
    }

    private void cancel() {
        close(); //TODO display alert
    }

    private void close() {
        getFragmentManager().beginTransaction().remove(this).commit();
    }

    private boolean save() {
        if(!checkInput()){
            return false;
        }

        Fragment parentFragment = getParentFragment();
        if(parentFragment instanceof FragConfigurations){
            AppData.getManager().addConfig(
                    this.tvConfigDesc.getText().toString(),
                    AppData.getManager().getTemperatures().get(this.cspTemperatures.getSelectedItemPosition()),
                    AppData.getManager().getDetergents().get(this.cspDetergents.getSelectedItemPosition())
            );

            close();
            return true;
        }
        return false;
    }

    private boolean checkInput(){
        boolean ok = true;
        //config desc
        String name = this.tvConfigDesc.getText().toString();
        if(this.tvConfigDesc.getText().length() <= 0){
            ok = false;
            TextView tv = this.dialogView.findViewById(R.id.NameError);
            tv.setText(R.string.config_desc_error_missing);
            this.llDescError.setVisibility(View.VISIBLE);
        } else if (AppData.getManager().configNameExists(name)){
            ok = false;
            TextView tv = this.dialogView.findViewById(R.id.NameError);
            tv.setText(R.string.config_desc_error_exists);
            this.llDescError.setVisibility(View.VISIBLE);
        }
        else{
            this.llDescError.setVisibility(View.GONE);
        }

        //temp
        if(this.cspTemperatures.getSelectedItemPosition() < 0){
            ok = false;
            this.llTempError.setVisibility(View.VISIBLE);
        }
        else{
            this.llTempError.setVisibility(View.GONE);
        }

        //detergent
        if(this.cspDetergents.getSelectedItemPosition() < 0){
            ok = false;
            this.llDetError.setVisibility(View.VISIBLE);
        }
        else{
            this.llDetError.setVisibility(View.GONE);
        }

        if(!ok){
            Toast.makeText(getContext(),getResources().getString(R.string.input_error),Toast.LENGTH_LONG).show();
        }

        return ok;
    }

    /** The system calls this only when creating the layout in a dialog. */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // The only reason you might override this method when using onCreateView() is
        // to modify any dialog characteristics. For example, the dialog includes a
        // title by default, but your custom layout might not need it. So here you can
        // remove the dialog title, but you must call the superclass to get the Dialog.
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        //dialog.requestWindowFeature(Window.FEATURE_ACTION_BAR);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Dialog);
    }
}
