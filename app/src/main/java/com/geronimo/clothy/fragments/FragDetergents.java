package com.geronimo.clothy.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.Toast;

import com.geronimo.clothy.AppData;
import com.geronimo.clothy.R;
import com.geronimo.clothy.data.Detergent;
import com.geronimo.clothy.util.InUseException;
import com.geronimo.clothy.util.MyRecyclerScroll;
import com.geronimo.clothy.util.displayDetergents.DetergentAdapter;
import com.geronimo.clothy.util.displayDetergents.DetergentSwipeHelper;
import com.geronimo.clothy.util.displayStrings.SingleStringAdapter;
import com.geronimo.clothy.util.displayStrings.SingleStringSwipeHelper;
import com.geronimo.clothy.util.displayStrings.SingleStringViewHolder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FragDetergents extends Fragment
        implements DetergentSwipeHelper.DetergentSwipeHelperListener {

    private static DetergentAdapter detAdapter;
    private RecyclerView recyclerView;
    private View fragmentView;
    private FloatingActionButton fab;
    private DetergentSwipeHelper swipeHelper;
    private static final int GET_DET_REQUEST_CODE = 1;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //restore(savedInstanceState);

        // Inflate the layout for this fragment
        this.fragmentView = inflater.inflate(R.layout.frag_string_list, container, false);
        this.recyclerView = this.fragmentView.findViewById(R.id.listTwoLines);
        this.fab = this.fragmentView.findViewById(R.id.fabAdd);

        setHasOptionsMenu(false);

        setUpFabBehaviour();

        setUpList();

        return this.fragmentView;
    }

    private void setUpList(){
        // use a linear layout manager
        this.recyclerView.setLayoutManager(
                new LinearLayoutManager(getContext())
        );
        //this.recyclerView.setItemAnimator(new DefaultItemAnimator());

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        this.recyclerView.setHasFixedSize(true);

        //divider
        this.recyclerView.addItemDecoration(
                new DividerItemDecoration(recyclerView.getContext(), LinearLayoutManager.VERTICAL)
        );

        //adapter
        setDetergentAdapter(new DetergentAdapter(
                AppData.getManager().getDetergents()
        ));

        this.recyclerView.setAdapter(detAdapter);


        //register swipes
        this.swipeHelper = new DetergentSwipeHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(this.swipeHelper).attachToRecyclerView(this.recyclerView);
        enableItemSwipe(true);
    }

    private void setDetergentAdapter(DetergentAdapter adapter){
        detAdapter = adapter;
        AppData.getManager().setDetAdapter(detAdapter);
    }

    private void setUpFabBehaviour(){
        //hide upon scrolling down and reappear when scrolled up (known as the Quick Return Pattern)
        final int fabMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        recyclerView.addOnScrollListener(new MyRecyclerScroll() {
            @Override
            public void show() {
                fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                fab.animate().translationY(fab.getHeight() + fabMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabClick();
            }
        });
    }

    private void fabClick(){
        Bundle bundle = new Bundle();
        bundle.putSerializable("Type", MyDialog.MyDialogType.ADD_DET);
        FragmentManager fragmentManager = getFragmentManager();
        MyDialog d = new MyDialog();
        d.setArguments(bundle);
        d.setTargetFragment(FragDetergents.this, GET_DET_REQUEST_CODE);
        d.show(fragmentManager, "addDet");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if( resultCode != MyDialog.RESULT_OK ) {
            return;
        }
        if( requestCode == GET_DET_REQUEST_CODE) {
            String detName = data.getStringExtra("Detergent");
            int priority = data.getIntExtra("Priority", Integer.MIN_VALUE);
            Detergent newDet = new Detergent(detName, priority);

            if (detName.isEmpty() || priority == Integer.MIN_VALUE) {
                Toast.makeText(getContext(), getString(R.string.internal_error), Toast.LENGTH_SHORT).show();
            } else if (AppData.getManager().detExists(newDet)){
                Bundle bundle = new Bundle();
                bundle.putSerializable("Type", MyDialog.MyDialogType.CANT_ADD_DET);
                FragmentManager fragmentManager = getChildFragmentManager();
                MyDialog d = new MyDialog();
                d.setArguments(bundle);
                d.show(fragmentManager, "cantAddDet");
            } else{
                //add
                AppData.getManager().addDetergent(newDet);
            }
        }
    }

//    private void fabClick(){
//        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        builder.setTitle(getString(R.string.add_det_title));
//
//        // Set up the input
//        final EditText input = new EditText(getContext());
//        // Specify the type of input expected
//        input.setInputType(InputType.TYPE_CLASS_TEXT);
//        builder.setView(input);
//
//        // Set up the buttons
//        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                String newDet = input.getText().toString();
//                if (AppData.getManager().detExists(newDet)){
//                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
//                    String msg = getString(R.string.det_exists) + System.lineSeparator()
//                            + getString(R.string.add_not_possible);
//                    dlgAlert.setMessage(msg);
//                    dlgAlert.setTitle(getString(R.string.attention));
//                    dlgAlert.setPositiveButton(getString(R.string.ok), null);
//                    dlgAlert.setCancelable(false);
//                    dlgAlert.setIcon(R.drawable.ic_warning);
//                    dlgAlert.create().show();
//                } else{
//                    //add
//                    AppData.getManager().addDetergent(newDet);
//                }
//            }
//        });
//        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//        builder.show();
//    }

    /**
     * callback when recycler view is swiped
     * item will be removed on swiped
     * undo option will be provided in snackbar to restore the item
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof SingleStringViewHolder) {
            int adapterPos = viewHolder.getAdapterPosition();
            // backup of removed item for undo purpose
            final int deletedIndex = viewHolder.getAdapterPosition();
            final Detergent deletedItem = AppData.getManager().getDetergents().get(deletedIndex);

            // remove the item
            try {
                AppData.getManager().removeDetergent(adapterPos);
            } catch (InUseException e){
                FragmentManager fragmentManager = getChildFragmentManager();
                MyDialog dialog = new MyDialog();
                Bundle bundle = new Bundle();
                bundle.putSerializable("Type", MyDialog.MyDialogType.CANT_DELETE_DET);
                dialog.setArguments(bundle);
                dialog.show(fragmentManager, "cantDeleteDet");
//                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
//                String msg = getString(R.string.det_in_use) + System.lineSeparator()
//                        + getString(R.string.remove_not_possible);
//                dlgAlert.setMessage(msg);
//                dlgAlert.setTitle(getString(R.string.attention));
//                dlgAlert.setPositiveButton(getString(R.string.ok), null);
//                dlgAlert.setCancelable(false);
//                dlgAlert.setIcon(R.drawable.ic_warning);
//                dlgAlert.create().show();
                detAdapter.notifyDataSetChanged();
                return;
            }
            //_recyclerViewAdapter.notifyItemRemoved(deletedIndex);

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(fragmentView, getResources().getString(R.string.item_removed), Snackbar.LENGTH_LONG);
            snackbar.setAction(getResources().getString(R.string.undo), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // undo is selected, restore the deleted item
                    // _recyclerViewAdapter.notifyItemInserted(AppData.getFreezerManager().addFrozenItem(deletedItem));
                    AppData.getManager().addDetergent(deletedItem);
                }
            });
            snackbar.show();
        }
    }

    private void enableItemSwipe(boolean enable){
        if (this.swipeHelper != null) {
            this.swipeHelper.setSwipeEnabled(enable);
        }
    }
}
