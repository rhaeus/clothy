package com.geronimo.clothy;

import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.geronimo.clothy.fragments.FragConfigurations;
import com.geronimo.clothy.fragments.FragTemperatures;
import com.geronimo.clothy.fragments.FragDetergents;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppData.readAppData(getFilesDir().getAbsolutePath());

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if(savedInstanceState == null) {
            navigationView.getMenu().getItem(0).setChecked(true);
            showFragment(R.id.nav_config);
//            setTitle(R.string.nav_item_all);
//            Fragment_All all = new Fragment_All();
//            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, all).commit();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // set item as selected to persist highlight
        item.setChecked(true);

        // Handle navigation view item clicks here.
        showFragment(item.getItemId());

        // close drawer when item is tapped
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showFragment(int menuID)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch(menuID) {
            case R.id.nav_config:
                getSupportActionBar().setTitle(R.string.nav_menu_config);
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, new FragConfigurations())
                        .addToBackStack("CONFIGURATIONS")
                        .commit();
                break;
            case R.id.nav_temp:
                getSupportActionBar().setTitle(R.string.nav_menu_temp);
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, new FragTemperatures())
                        .addToBackStack("TEMPERATURES")
                        .commit();
                break;
            case R.id.nav_det:
                getSupportActionBar().setTitle(R.string.nav_menu_detergents);
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, new FragDetergents())
                        .addToBackStack("DETERGENTS")
                        .commit();
                break;
        }
    }

    @Override
    public void onStop(){
        super.onStop();

        AppData.writeAppData(getFilesDir().getAbsolutePath());
    }

    @Override
    public void onStart(){
        super.onStart();

        //AppData.readAppData(getFilesDir().getAbsolutePath());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("Title", getSupportActionBar().getTitle().toString());
    }

    @Override
    public void onRestoreInstanceState(Bundle inState){
        getSupportActionBar().setTitle(inState.getString("Title"));
    }
}
