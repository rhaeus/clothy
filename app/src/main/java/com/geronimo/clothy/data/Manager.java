package com.geronimo.clothy.data;

import com.geronimo.clothy.util.InUseException;
import com.geronimo.clothy.util.displayConfigurations.ConfigAdapter;
import com.geronimo.clothy.util.displayConfigurations.ConfigComparator;
import com.geronimo.clothy.util.displayDetergents.DetergentAdapter;
import com.geronimo.clothy.util.displayStrings.SingleStringAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root
public class Manager {
    @ElementList
    private List<Configuration> configs;
    @ElementList
    private List<Integer> temperatures;
    @ElementList
    private List<Detergent> detergents;
    @Element
    private ConfigOrder lastSortOrder = ConfigOrder.NAME_ASC;

    private ConfigAdapter configAdapter;
    private SingleStringAdapter tempAdapter;
    private DetergentAdapter detAdapter;

    public Manager(){
        this.configs = new ArrayList<>();
        this.temperatures = new ArrayList<>();
        this.detergents = new ArrayList<>();
    }

    public void generateDummyData(){
        this.detergents.add(new Detergent("Normal", 1));
        this.detergents.add(new Detergent("Schwarz", 2));
        this.detergents.add(new Detergent("Bunt", 2));

        this.temperatures.add(30);
        this.temperatures.add(40);
        this.temperatures.add(95);

        this.configs.add(new Configuration(getNewItemID(), "Normal", temperatures.get(0), detergents.get(0)));
        this.configs.add(new Configuration(getNewItemID(), "Schwarz", temperatures.get(2), detergents.get(1)));
        this.configs.add(new Configuration(getNewItemID(), "Bunt", temperatures.get(1), detergents.get(2)));

        sortConfigs(lastSortOrder);
    }

    public int addConfig(Configuration item){
        configs.add(item);
        sortConfigs(lastSortOrder);
        //update recyclerview
        updateConfigAdapterData();
        return this.configs.indexOf(item);
    }

    public void addConfigs(List<Configuration> items){
        Objects.requireNonNull(items);

        this.configs.addAll(items);

        sortConfigs(lastSortOrder);
        //update recyclerview
        updateConfigAdapterData();
    }

    //returns index where added, -1 if not valid
    public int addConfig(String name, int temp, Detergent detergent){
        if (!this.temperatures.contains(temp) || !this.detergents.contains(detergent)){
            return -1;
        }

        Configuration newConfig = new Configuration(getNewItemID(), name, temp, detergent);
        configs.add(newConfig);
        sortConfigs(lastSortOrder);
        //update recyclerview
        updateConfigAdapterData();
        return this.configs.indexOf(newConfig);
    }

    public void sortConfigs(ConfigOrder sorting){
        ConfigComparator comparator = new ConfigComparator(sorting);
        Collections.sort(this.configs, comparator);
        this.lastSortOrder = sorting;
        updateConfigAdapterData();
    }

    //returns index where inserted
    public int addDetergent(Detergent item){
        if (detExists(item)){
            return -1; //TODO evtl exception?
        }

        this.detergents.add(item);
        Collections.sort(this.detergents);
        //update recyclerview
        updateDetAdapterData();
        return this.detergents.indexOf(item);
    }

    //returns removed temperature
    public Detergent removeDetergent(int position) throws InUseException{
        if (position < 0 || position >= this.detergents.size()){
            throw new IllegalArgumentException("Argument 'position' out of range.");
        }

        if (isDetUsedByConfig(this.detergents.get(position))){
            throw new InUseException("Detergent is in use.");
        }

        Detergent removed = this.detergents.remove(position);

        //update recyclerview
        updateDetAdapterData();

        return removed;
    }

    //returns index where inserted
    public int addTemp(int item){
        if (tempExists(item)){
            return -1; //TODO evtl exception?
        }

        this.temperatures.add(item);
        Collections.sort(this.temperatures);
        //update recyclerview
        updateTempAdapterData();
        return this.temperatures.indexOf(item);
    }

    //returns removed temperature
    public int removeTemp(int position) throws InUseException{
        if (position < 0 || position >= this.temperatures.size()){
            throw new IllegalArgumentException("Argument 'position' out of range.");
        }

        if (isTempUsedByConfig(this.temperatures.get(position))){
            throw new InUseException("Temperature is in use.");
        }

        int removed = this.temperatures.remove(position);

        //update recyclerview
        updateTempAdapterData();

        return removed;
    }

    public boolean isTempUsedByConfig(int temp){
        for (Configuration config : this.configs){
            if (config.getTemperature() == temp){
                return true;
            }
        }
        return false;
    }

    public boolean isDetUsedByConfig(Detergent det){
        for (Configuration config : this.configs){
            if (det.equals(config.getDetergent())){
                return true;
            }
        }
        return false;
    }

    //returns removed Configuration
    public Configuration removeConfig(int position){
        if (position < 0 || position >= this.configs.size()){
            throw new IllegalArgumentException("Argument 'position' out of range.");
        }
        Configuration removed = this.configs.remove(position);

        //update recyclerview
        updateConfigAdapterData();

        return removed;
    }
    public List<Configuration> removeConfigs(List<Long> positions){
        Objects.requireNonNull(positions);
        List<Configuration> removed = new ArrayList<>();

        for (long position : positions){
            if (position < 0 || position >= this.configs.size()){
                throw new IllegalArgumentException("Argument 'positions' out of range.");
            }
            removed.add(this.configs.get((int)position));
        }

        for (Configuration config : removed){
            this.configs.remove(config);
        }

        updateConfigAdapterData();

        return removed;
    }

    public List<Configuration> getConfigs(){
        return this.configs;
    }

    public List<String> getTemperaturesWithUnit(){
        List<String> temps = new ArrayList<>();
        for (Integer temp : this.temperatures){
            temps.add(temp + "°C");
        }
        return temps;
    }


    public List<Integer> getTemperatures() {
        return this.temperatures;
    }

    public List<Detergent> getDetergents(){
        return this.detergents;
    }

    public ConfigOrder getLastSortOrder(){
        return this.lastSortOrder;
    }


    public long getNewItemID(){
        boolean idExists = false;
        for(long id = 0; id < Long.MAX_VALUE; id++){
            idExists = false;
            for (Configuration config : this.configs){
                if (config.getId() == id){
                    idExists = true;
                    break;
                }
            }

            if(!idExists)
                return id;
        }
        return -1;
    }

    public String evaluate(List<Long> selected){
        int minTemp = Integer.MAX_VALUE;
        int maxTemp = Integer.MIN_VALUE;
        int minPriority = Integer.MAX_VALUE;

        String sel = "";

        for (int i = 0; i < selected.size(); i++){
            Configuration config = configs.get(selected.get(i).intValue());
            sel += config.getName();
            if (config.getTemperature() < minTemp){
                minTemp = config.getTemperature();
            }

            if (config.getTemperature() > maxTemp){
                maxTemp = config.getTemperature();
            }

            if (config.getDetergent().getPriority() < minPriority){
                minPriority = config.getDetergent().getPriority();
            }
        }

        //collect all detergents with minPriority
        List<Detergent> dets = new ArrayList<>();
        for (int i = 0; i < selected.size(); i++){
            Configuration config = configs.get(selected.get(i).intValue());
            if (config.getDetergent().getPriority() == minPriority){
                dets.add(config.getDetergent());
            }
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < dets.size(); i++){
            sb.append(dets.get(i).toString());
            if (i < dets.size() - 1){
                sb.append(", ");
            }
        }

        String s = sb.toString();
        //return sel;
        return "Empfohlene Temperatur: " + minTemp + "°C" + System.lineSeparator()
                + "Empfohlene(s) Waschmittel: " + sb.toString()

                + System.lineSeparator() + System.lineSeparator()
                 + "Temperaturbereich: " + minTemp + "°C - " + maxTemp +  "°C";

    }

    public boolean configNameExists(String name){
        for (Configuration config : this.configs){
            if (name.equals(config.getName())){
                return true;
            }
        }
        return false;
    }

    public boolean tempExists(int temp){
        return this.temperatures.contains(temp);
    }

    public boolean detExists(Detergent det){
        return this.detergents.contains(det);
    }

    public void setConfigAdapter(ConfigAdapter adapter){
        this.configAdapter = adapter;
    }
    private void updateConfigAdapterData(){
        if (this.configAdapter != null) {
            this.configAdapter.renewList(this.configs);
        }
    }

    public void setTempAdapter(SingleStringAdapter adapter){
        this.tempAdapter = adapter;
    }
    private void updateTempAdapterData(){
        if (this.tempAdapter != null) {
            this.tempAdapter.renewList(getTemperaturesWithUnit());
        }
    }

    public void setDetAdapter(DetergentAdapter adapter){
        this.detAdapter = adapter;
    }
    private void updateDetAdapterData(){
        if (this.detAdapter != null) {
            this.detAdapter.renewList(getDetergents());
        }
    }
}
