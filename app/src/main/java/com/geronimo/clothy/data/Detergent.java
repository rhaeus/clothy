package com.geronimo.clothy.data;

import java.util.Objects;

public class Detergent implements Comparable<Detergent>{
    private String name;
    private int priority = Integer.MIN_VALUE;

    //for serialization
    public Detergent(){}

    public Detergent(String name, int priority){
        this.name = name;
        this.priority = priority;
    }

    public String getName(){
        return this.name;
    }

    public int getPriority(){
        return this.priority;
    }


    @Override
    public int compareTo(Detergent o) {
        if (name.equals(o.name))
            return priority - o.priority;
        return name.compareTo(o.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Detergent detergent = (Detergent) o;
        return priority == detergent.priority &&
                Objects.equals(name, detergent.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, priority);
    }

    @Override
    public String toString() {
        return name +  " (" + priority + ")";
    }
}
